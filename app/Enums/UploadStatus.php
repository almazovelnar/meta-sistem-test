<?php
declare(strict_types=1);

namespace App\Enums;

enum UploadStatus: string
{
    case IN_PROGRESS = 'in_progress';
    case SUCCESS = 'success';
    case FAILED = 'failed';
}

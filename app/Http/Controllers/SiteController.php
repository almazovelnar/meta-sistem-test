<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Exception;
use App\Enums\UploadStatus;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Storage;

class SiteController extends Controller
{
    public function index(): View
    {
        return view('home');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request): JsonResponse
    {
        try {
            $resumableIdentifier  = $request->input('resumableIdentifier');
            $resumableFilename    = $request->input('resumableFilename');
            $resumableChunkNumber = $request->input('resumableChunkNumber');
            $resumableTotalChunks = $request->input('resumableTotalChunks');

            $resumableDirPath = "uploads/{$resumableIdentifier}";

            // Ensure the directory for this identifier exists
            if (!Storage::exists($resumableDirPath)) {
                Storage::makeDirectory($resumableDirPath);
            }

            $request->file('file')->storeAs($resumableDirPath, "{$resumableFilename}_{$resumableChunkNumber}");

            // Get count uploaded chunks
            $uploadedChunks = count(Storage::files($resumableDirPath));

            // Check if all chunks are uploaded
            if ($uploadedChunks == $resumableTotalChunks) {
                // Combine chunks into the final file
                $this->combineChunks($resumableDirPath, $resumableFilename, $resumableTotalChunks);

                // Remove dir with chunks
                Storage::deleteDirectory($resumableDirPath);

                return response()->json([
                    'status' => UploadStatus::SUCCESS->value
                ]);
            }

            return response()->json([
                'status' => UploadStatus::IN_PROGRESS->value
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => UploadStatus::FAILED->value,
                'error'  => $e->getMessage()
            ]);
        }
    }

    /**
     * After upload all chunk files, combine their in one file
     *
     * @param $directory
     * @param $filename
     * @param $totalChunks
     * @return void
     */
    private function combineChunks($directory, $filename, $totalChunks): void
    {
        $outputPath = storage_path("app/public/uploads/{$filename}");

        for ($i = 1; $i <= $totalChunks; $i++) {
            $chunkPath    = storage_path("app/public/{$directory}/{$filename}_{$i}");
            $chunkContent = file_get_contents($chunkPath);
            file_put_contents($outputPath, $chunkContent, FILE_APPEND);
            Storage::delete("{$directory}/{$filename}_{$i}");
        }
    }
}

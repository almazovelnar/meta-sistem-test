<script src="{{ asset('assets/js/resumable.js') }}"></script>
<script>
    var fileLabel = document.getElementById('fileSpan');
    document.addEventListener('DOMContentLoaded', function () {
        var r = new Resumable({
            target: '{{ route('fileUpload') }}',
            chunkSize: 1024 * 1024, // 1MB chunks
            simultaneousUploads: 3,
            testChunks: false,
            query: {
                _token: '{{ csrf_token() }}'
            }
        });

        r.assignDrop(document.getElementById('fileLabel'));

        r.on('fileAdded', function (file) {
            fileLabel.innerHTML = file.fileName + "<br> File is uploading..."
            r.upload();
        });

        r.on('fileSuccess', function (file) {
            fileLabel.innerHTML = file.fileName + "<br> File upload successful!"
        });

        r.on('fileError', function (file, message) {
            fileLabel.innerHTML = "Error!"
            console.error('File upload error:', file, message);
        });
    });
</script>
